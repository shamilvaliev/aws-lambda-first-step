package com.tutorial;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tutorial.dto.InputObject;
import com.tutorial.dto.OutputObject;

public class TutorialRequestHandler implements RequestHandler<InputObject, OutputObject> {

    @Override
    public OutputObject handleRequest(InputObject input, Context context) {
        OutputObject outputObject = new OutputObject();
        outputObject.setResult(String.format("Request handler received %s and %s", input.getCode(), input.getName()));
        return outputObject;
    }
}
