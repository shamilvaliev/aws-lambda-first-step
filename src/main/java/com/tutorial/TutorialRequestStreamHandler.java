package com.tutorial;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.tutorial.dto.InputObject;
import com.tutorial.dto.OutputObject;

import java.io.*;
import java.nio.charset.Charset;

public class TutorialRequestStreamHandler implements RequestStreamHandler {

    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
        LambdaLogger logger = context.getLogger();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, Charset.forName("US-ASCII")));
        PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(output, Charset.forName("US-ASCII"))));
        try {
            InputObject inputEvent = gson.fromJson(reader, InputObject.class);
            OutputObject outputObject = new OutputObject();
            outputObject.setResult(String.format("Request stream handler received %s and %s", inputEvent.getCode(), inputEvent.getName()));
            writer.write(gson.toJson(outputObject));
            if (writer.checkError()) {
                logger.log("WARNING: Writer encountered an error.");
            }
        } catch (IllegalStateException | JsonSyntaxException exception) {
            logger.log(exception.toString());
        } finally {
            reader.close();
            writer.close();
        }
    }
}
